from driver import compute
from values import *
from defs import *
import electrochemical 
import water
import glucose
import cotransport
import NHE3
import ATPase
import NKCC
import KCC
import NCC
import ENaC
import Pendrin
import AE1
import NHE1
import flux
import os
import argparse

solute = ['Na','K','Cl','HCO3','H2CO3','CO2','HPO4','H2PO4','urea','NH3','NH4','H','HCO2','H2CO2','glu']
compart = ['Lumen','Cell','ICA','ICB','LIS','Bath']
cw=Vref*60e6

sup_segments=['PT','S3','SDL','mTAL','cTAL','MD','DCT','CNT']
jux_segments=['PT','S3','SDL','LDL','LAL','mTAL','cTAL','MD','DCT','CNT']
types = ['sup','jux1','jux2','jux3','jux4','jux5']

#gender=input('Which gender do you want to simulate? (Male or Female) ')

#diabete=input('Dose it have diabete? (Y/N) ')

#inhib=input('Any transport inhibition? (nhe3 50%/ nhe3 80%/ NKCC2 70%/ NKCC2 100%) ')

parser = argparse.ArgumentParser()
parser.add_argument('--sex',choices=['Male','Female'],required = True,type = str,help = 'sex of rat')
parser.add_argument('--species',choices=['human','rat'],required = True,type = str, help = 'Human model or Rat model')
parser.add_argument('--inhibition',choices=['ACE'],default = None,type = str,help = 'any transporter inhibition')
args = parser.parse_args()
gender = args.sex
humOrrat = args.species
inhib = args.inhibition

diabete = 'N'

begin=input('Which segment do you want to start with? ')
end=input('Which segment do you want to stop with? ')
sup_or_jux=input('Is this superfical or juxtamedullary? (sup/jux1/jux2/jux3/jux4/jux5) ')
file_to_save=input('Input a file name to save data: ')
if os.path.isdir(file_to_save) == False:
    os.makedirs(file_to_save)

if sup_or_jux == 'sup':
    segments = sup_segments
else:
    segments = jux_segments

begin_index=segments.index(begin)
end_index=segments.index(end)

for segment in segments[begin_index:end_index+1]:
    if gender == 'Male':
        filename = segment+'params_M_'+humOrrat[0:3]+'.dat'
    elif gender == 'Female':
        filename = segment+'params_F_'+humOrrat[0:3]+'.dat'
    else:
        print('Did you choose correct segments?')

    if segment == 'PT':
        N = 181
    elif segment == 'S3':
        N = 20
    elif segment == 'MD':
        N = 2
    else:
        N = 200

    if gender == 'Male':
        if segment == 'PT':#or segment == 'SDL':# or segment == 'mTAL' or segment == 'DCT':
            method = 'Broyden'
        else:
            method = 'Newton'
    elif gender == 'Female':
        if segment == 'PT' or segment == 'DCT': # for nhe3 50: mTAL can't use broyden
            if (segment == 'DCT' and sup_or_jux == 'sup') or (segment=='DCT' and sup_or_jux == 'jux5'):
                method = 'Newton'
            else:
                method = 'Broyden'
        else:
            method = 'Newton'
    cell = compute(N,filename,method,sup_or_jux,diabete,humOrrat,inhibition = inhib)
    for i in range(NS):
        file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_con_of_'+solute[i]+'_in_Lumen_'+sup_or_jux+'.txt','w')
        for j in range(N):
            file.write(str(cell[j].conc[i,0])+'\n')
        file.close()
    for i in range(NS):
        file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_con_of_'+solute[i]+'_in_Cell_'+sup_or_jux+'.txt','w')
        for j in range(N):
            file.write(str(cell[j].conc[i,1])+'\n')
        file.close()
    for i in range(NS):
        file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_con_of_'+solute[i]+'_in_Bath_'+sup_or_jux+'.txt','w')
        for j in range(N):
            file.write(str(cell[j].conc[i,5])+'\n')
        file.close()

    file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_water_volume_in_Lumen_'+sup_or_jux+'.txt','w')
    for j in range(N):
        file.write(str(cell[j].vol[0]*cw)+'\n')
    file.close()
    file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_water_volume_in_Cell_'+sup_or_jux+'.txt','w')
    for j in range(N):
        file.write(str(cell[j].vol[1]*cw)+'\n')
    file.close()

    for i in range(NS):
        file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_flow_of_'+solute[i]+'_in_Lumen_'+sup_or_jux+'.txt','w')
        for j in range(N):
            file.write(str(cell[j].conc[i,0]*cell[j].vol[0]*cw)+'\n')
        file.close()
    for i in range(NS):
        file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_flow_of_'+solute[i]+'_in_Cell_'+sup_or_jux+'.txt','w')
        for j in range(N):
            file.write(str(cell[j].conc[i,1]*cell[j].vol[1]*cw)+'\n')
        file.close()

    file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_pH_in_Lumen_'+sup_or_jux+'.txt','w')
    for j in range(N):
        file.write(str(-np.log(cell[j].conc[11,0]/1000)/np.log(10))+'\n')
    file.close()

    file=open('./'+file_to_save+'/'+cell[0].sex+cell[0].segment+'_pressure_in_Lumen_'+sup_or_jux+'.txt','w')
    for j in range(N):
        file.write(str(cell[j].pres[0])+'\n')
    file.close()

    for j in range(N):
        for i in range(len(cell[j].trans)):
            transporter_type = cell[j].trans[i].type
            memb_id = cell[j].trans[i].membrane_id

            if transporter_type == 'SGLT1':
                solute_id,fluxs = glucose.sglt1(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'SGLT2':
                solute_id,fluxs = glucose.sglt2(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'GLUT1':
                solute_id,fluxs=glucose.glut1(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len([solute_id])):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id]+str(memb_id[0])+str(memb_id[1])+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs)+'\n')
            elif transporter_type == 'GLUT2':
                solute_id,fluxs=glucose.glut2(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len([solute_id])):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id]+str(memb_id[0])+str(memb_id[1])+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs)+'\n')			
            elif transporter_type == 'NHE3':
                solute_id,fluxs=NHE3.nhe3(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NaKATPase':
                solute_id,fluxs=ATPase.nakatpase(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+str(memb_id[0])+str(memb_id[1])+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')

            elif transporter_type == 'HATPase':
                solute_id,fluxs=ATPase.hatpase(cell[j],cell[j].ep,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NKCC2A':
                solute_id,fluxs=NKCC.nkcc2(cell[j],memb_id,cell[j].trans[i].act,cell[j].area,'A')
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NKCC2B':
                solute_id,fluxs=NKCC.nkcc2(cell[j],memb_id,cell[j].trans[i].act,cell[j].area,'B')
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NKCC2F':
                solute_id,fluxs=NKCC.nkcc2(cell[j],memb_id,cell[j].trans[i].act,cell[j].area,'F')
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')       
            elif transporter_type == 'KCC4':
                solute_id,fluxs=KCC.kcc4(cell[j].conc,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'ENaC':
                solute_id,fluxs=ENaC.ENaC(cell[j],j,memb_id,cell[j].trans[i].act,cell[j].area,jvol)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NCC':
                solute_id,fluxs=NCC.NCC(cell[j],j,memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'Pendrin':
                solute_id,fluxs=Pendrin.Pendrin(cell[j],memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type =='AE1':
                solute_id,fluxs=AE1.AE1(cell[j],memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'HKATPase':
                solute_id,fluxs=ATPase.hkatpase(cell[j],memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NHE1':
                solute_id,fluxs=NHE1.NHE1(cell[j],memb_id,cell[j].trans[i].act,cell[j].area)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            elif transporter_type == 'NKCC1':
                solute_id,fluxs=NKCC.nkcc1(cell[j],memb_id,cell[j].trans[i].act,delmu)
                for k in range(len(solute_id)):
                    file = open('./'+file_to_save+'/'+cell[j].sex+'_'+humOrrat[0:3]+'_'+cell[j].segment+'_'+transporter_type+'_'+solute[solute_id[k]]+'_'+sup_or_jux+'.txt','a')
                    file.write(str(fluxs[k])+'\n')
            else:
                print('What is this?',transporter_type)	
    print(segment+' finished')